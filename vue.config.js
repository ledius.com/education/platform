module.exports = {
  transpileDependencies: ['vuetify'],

  pluginOptions: {
    i18n: {
      locale: 'ru',
      fallbackLocale: 'ru',
      localeDir: 'locales',
      enableInSFC: true
    }
  },

  devServer: {
    proxy: {
      '^/api': {
        target: process.env.PROXY_PASS
      }
    }
  },

  pwa: {
    name: 'Ledius',
    short_name: 'Ledius',
    assetsVersion: '1.0.10'
  },

  pages: {
    index: {
      entry: 'src/main.ts',
      template: 'public/index.html',
      filename: 'index.html',
      title: 'Ledius'
    }
  }
};
