import { Configuration, ConfigurationParameters } from '@/api/ledius-token';

export function configuration(
  parameters: ConfigurationParameters = {}
): Configuration {
  return new Configuration({
    basePath: `${window.location.protocol}//${window.location.host}`,
    accessToken: '',
    ...parameters
  });
}
