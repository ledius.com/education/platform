export async function safeCall(
  ...calls: (() => Promise<unknown>)[]
): Promise<void> {
  for (const call of calls) {
    try {
      await call();
    } catch (e) {
      console.error('safe call error', e);
    }
  }
}

export function formatBalance(balance: string, decimals: number = 18): string {
  const [scale, precision] = [
    BigInt(balance) / BigInt(10 ** decimals),
    balance
      .split('')
      .slice(-18)
      .join('')
      .padStart(decimals, '0')
  ];
  return `${scale}.${precision}`;
}

export function fromScaledAmount(
  amount: string,
  decimals: number = 18
): string {
  return formatBalance(amount, decimals);
}

export function toScaledBalance(balance: string): string {
  const [scale, precision = '000000000000000000'] = balance.split('.');

  return BigInt(`${scale}${precision}`).toString();
}
