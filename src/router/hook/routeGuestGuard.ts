import { NavigationGuardNext, Route } from "vue-router";
import Vue from "vue";
import store from "@/store";

export const routeGuestGuard = (to: Route, from: Route, next: NavigationGuardNext<Vue>): void => {
  if (to.matched.some(route => route.meta.requiredGuest)) {
    if (!store.getters['auth/isAuth']) {
      next();
    } else {
      next({
        name: "Courses"
      });
    }
  } else {
    next();
  }
};
