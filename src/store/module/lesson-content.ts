import { LessonContent } from "@/domain/entity/lesson/LessonContent";
import { Module } from "vuex";
import { RootState } from "@/store";
import axios from "axios";
import { LessonResponse } from "@/domain/response/lessons/LessonResponse";

type State = {
  lesson: LessonContent|null;
};

export const lessonContent = {
  namespaced: true,
  state: {
    lesson: null
  },
  getters: {
    lesson: (state): LessonContent|null => state.lesson
  },
  mutations: {
    set: (state: State, payload: LessonContent) => {
      state.lesson = payload;
    }
  },
  actions: {
    fetch: async ({ commit }, id: string): Promise<void> => {
      const { data } = await axios.get<LessonResponse>(`/api/v1/lesson/${id}`);
      commit('set', LessonContent.ofLessonResponse(data));
    }
  }
} as Module<State, RootState>;
