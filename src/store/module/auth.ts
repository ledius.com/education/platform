import { Module } from 'vuex';
import {
  AuthenticationApi,
  AuthInfoResponse,
  JoinCommand,
  LoginConfirmResponse,
  LoginEmailConfirmCommand,
  LoginEmailRequestCommand,
  RefreshResponse
} from '@/api/courses';
import { configuration } from '@/api/configuration';

const authenticationApi = new AuthenticationApi(configuration());

type State = {
  isAuth: boolean;
  token: LoginConfirmResponse | RefreshResponse | null;
  info: AuthInfoResponse | null;
};

export default {
  namespaced: true,
  state: {
    isAuth: false,
    token: null,
    info: null
  },
  getters: {
    isAuth: (state: State): boolean => state.isAuth,
    token: (state: State): LoginConfirmResponse | RefreshResponse | null =>
      state.token,
    info: (state: State): AuthInfoResponse | null => state.info
  },
  actions: {
    fetchInfo: async ({ state }): Promise<void> => {
      state.info = await authenticationApi.getInfo().then(({ data }) => data);
    },
    check: async ({ state, dispatch }): Promise<void> => {
      const authorization = window.localStorage.getItem('authorization');
      if (authorization === null) {
        state.isAuth = false;
        state.token = null;
      } else {
        try {
          const decoded = JSON.parse(authorization);
          state.isAuth = true;
          state.token = decoded;
          await dispatch('fetchInfo');
        } catch (e) {
          await dispatch('logout');
        }
      }
    },
    join: async (_, payload: JoinCommand): Promise<void> => {
      await authenticationApi.join({ joinCommand: payload });
    },
    request: async (
      _,
      loginEmailRequestCommand: LoginEmailRequestCommand
    ): Promise<void> => {
      await authenticationApi.requestByEmail({
        loginEmailRequestCommand
      });
    },
    confirm: async (
      { state, dispatch },
      loginEmailConfirmCommand: LoginEmailConfirmCommand
    ): Promise<void> => {
      const data = await authenticationApi
        .confirm({ loginEmailConfirmCommand })
        .then(({ data }) => data);
      state.isAuth = true;
      state.token = data;
      await dispatch('fetchInfo');
      window.localStorage.setItem('authorization', JSON.stringify(data));
    },
    refresh: async ({ state, dispatch }): Promise<void> => {
      const token = state.token;
      if (!token) {
        return await dispatch('logout');
      }
      const data = await authenticationApi
        .refresh({ tokenRefreshCommand: { refresh: token.refreshToken } })
        .then(({ data }) => data);
      state.isAuth = true;
      state.token = data;
      await dispatch('fetchInfo');
      window.localStorage.setItem('authorization', JSON.stringify(data));
    },
    refreshByToken: async (
      { state, dispatch },
      refreshToken: string
    ): Promise<void> => {
      const data = await authenticationApi
        .refresh({
          tokenRefreshCommand: {
            refresh: refreshToken
          }
        })
        .then(({ data }) => data);
      state.isAuth = true;
      state.token = data;
      await dispatch('fetchInfo');
      window.localStorage.setItem('authorization', JSON.stringify(data));
    },
    logout: async ({ state }): Promise<void> => {
      state.isAuth = false;
      state.token = null;
      window.localStorage.removeItem('authorization');
    }
  }
} as Module<State, unknown>;
