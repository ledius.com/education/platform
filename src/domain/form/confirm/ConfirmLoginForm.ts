export class ConfirmLoginForm {
  public readonly email: string;
  public readonly code: string;

  constructor (email: string, code: string) {
    this.email = email;
    this.code = code;
  }
}
