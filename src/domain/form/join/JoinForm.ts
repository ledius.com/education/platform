export class JoinForm {
  public readonly email: string;
  public readonly phone: string;

  constructor (email: string, phone: string) {
    this.email = email;
    this.phone = phone;
  }
}
