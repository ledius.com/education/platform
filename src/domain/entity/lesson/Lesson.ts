export class Lesson {
  public readonly id: string;
  public readonly name: string;
  public readonly icon: string;
  public readonly courseId: string;

  constructor (id: string, name: string, icon: string, courseId: string) {
    this.id = id;
    this.name = name;
    this.icon = icon;
    this.courseId = courseId;
  }
}
