import { ProfileResponse } from "@/domain/response/profile/ProfileResponse";

export class Profile {
  public readonly id: string;
  public readonly userId: string;
  public readonly firstName: string;
  public readonly lastName: string;

  constructor (id: string, userId: string, firstName: string, lastName: string) {
    this.id = id;
    this.userId = userId;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public static from (response: ProfileResponse): Profile {
    return new Profile(response.id, response.userId, response.firstName, response.lastName);
  }
}
