import { Status } from "@/domain/entity/consultation/Status";
import { ConsultationResponse } from "@/domain/response/consultations/ConsultationResponse";

type ConsultationContent = {
  name: string;
  description: string;
  icon: string;
};

export class Consultation {
  public readonly id: string;
  public readonly content: ConsultationContent;
  public readonly price: number;
  public readonly status: Status;

  constructor (id: string, content: ConsultationContent, price: number, status: Status) {
    this.id = id;
    this.content = content;
    this.price = price;
    this.status = status;
  }

  public static from (response: ConsultationResponse): Consultation {
    return new Consultation(response.id, response.content, response.price, response.status);
  }
}
