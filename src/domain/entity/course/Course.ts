export class Course {
  public readonly id: string;
  public readonly icon: string;
  public readonly name: string;
  public readonly price: number;

  public constructor (id: string, icon: string, name: string, price: number) {
    this.id = id;
    this.icon = icon;
    this.name = name;
    this.price = price;
  }
}
