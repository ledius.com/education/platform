export enum Status {
  Pending = 'Pending',
  Paid = 'Paid',
  Completed = 'Completed',
}
