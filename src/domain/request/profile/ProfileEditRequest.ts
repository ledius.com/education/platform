export interface ProfileEditRequest {
  readonly id: string;
  readonly firstName: string;
  readonly lastName: string;
}
