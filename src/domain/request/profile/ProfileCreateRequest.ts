export interface ProfileCreateRequest {
  readonly userId: string;
  readonly firstName: string;
  readonly lastName: string;
}
