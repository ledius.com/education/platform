export interface AuthInfoResponse {
  id: string;
  email: string;
  phone: string;
}
