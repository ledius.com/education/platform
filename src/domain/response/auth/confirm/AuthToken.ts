import { AuthConfirmResponse } from "@/domain/response/auth/confirm/AuthConfirmResponse";

export class AuthToken implements AuthConfirmResponse {
  readonly accessToken: string;
  readonly expiresIn: string;
  readonly refreshToken: string;
  readonly type: string;

  constructor (access: string, expiresIn: string, refreshToken: string, type: string) {
    this.accessToken = access;
    this.expiresIn = expiresIn;
    this.refreshToken = refreshToken;
    this.type = type;
  }

  public static ofConfirmResponse (response: AuthConfirmResponse): AuthToken {
    return new AuthToken(
      response.accessToken,
      response.expiresIn,
      response.refreshToken,
      response.type
    );
  }

  public isExpired (now: Date = new Date()): boolean {
    return new Date(this.expiresIn) < now;
  }
}
