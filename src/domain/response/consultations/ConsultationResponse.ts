import { Status } from "@/domain/entity/consultation/Status";

export interface ConsultationResponse {
  readonly id: string;
  readonly content: {
    name: string;
    description: string;
    icon: string;
  };
  readonly price: number;
  readonly status: Status;
}
