export interface LessonResponse {
  id: string;
  content: {
    name: string;
    icon: string;
    text: string;
  },
  courseId: string;
  isPublished: boolean;
  isDraft: boolean;
}
