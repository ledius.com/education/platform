export interface PurchaseCourseItem {
  id: string;
  userId: string;
  courseId: string;
  isPaid: boolean;
}
