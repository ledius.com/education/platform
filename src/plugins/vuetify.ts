import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: true,
    themes: {
      light: {
        primary: '#12295d',
        background: '#2a488f',
        common: '#e3e2e2',
        secondary: '#263238',
        accent: '#3f51b5',
        error: '#ffeb3b',
        warning: '#03a9f4',
        info: '#cddc39',
        success: '#4caf50'
      },
      dark: {
        container: '#2E2E2E',
        wrapped: '#33375F',
        background: '#181818',
        primary: '#4A59E9',
        accent: '#ff9800',
        secondary: '#181818',
        success: '#65D73D'
      }
    }
  }
});
